# Awesome ERC

[![build status](https://git.rwth-aachen.de/yushin.washio/awesome-erc/badges/master/build.svg)](https://git.rwth-aachen.de/yushin.washio/awesome-erc/commits/master)
[![coverage report](https://git.rwth-aachen.de/yushin.washio/awesome-erc/badges/master/coverage.svg)](https://git.rwth-aachen.de/yushin.washio/awesome-erc/commits/master)

Awesome Elastic Resource Controller (just an example to demostrate
GitLab CI goodness)

For information about setting up a GitLab CI Runner within the OpenStack
infrastructure of E.ON ERC,
[look here.](https://git.rwth-aachen.de/acs/OpenStackCloudSystems/tree/master/GeneralTools/ContinousIntegration)

For the tutorial use image "gitlab-ci-tutorial (based on Ubuntu 16.04
Cloud Server)" (it has been set up by running
[tutorial-vm-setup.sh](tutorial-vm-setup.sh) and gitlab-ci-multi-runner
is already installed), run

```shell
sudo gitlab-ci-multi-runner register
```

and follow the instructions (use the docker executor; default image is
not relevant).  Then (after you forked this repository) run

```shell
cd awesome-erc
git remote set-url origin https://url-of-my-fork/awesome-erc.git
git config user.name 'My Name'
git config user.email 'my-email@rwth-aachen.de'
```

Unless you use the ssh address as remote, setting
`https://your.user.name@git.rwth-aachen.de/your.user.name/awesome-erc.git`
will save you from writing your user name every time.

Edit the build and coverage badge urls in README.md according to your
fork, also changing `master` to `my-tutorial` (the default branch name
checked out in the tutorial VM) so it'll show the status of your work.

After your first push, select the `my-tutorial` branch on the website
as well (or even set it as the default branch in Settings -> General ->
General project settings).

Proceed subsequent tutorial levels by `git cherry-pick tutorial-02` etc.
If you get `error: could not apply`, edit and add `.gitlab-ci.yml` and
run `git cherry-pick --continue`.

This project consists of three applications:

## simulate

A C++ app calculating total power demand from a list of demands of each
household

Build with:

```
make -C simulate
```

Run with:

```
simulate <demand values>...
```

## fiCity

A python app calculating total power demand of a city from context data
from the context broker, requires simulate

Run with:

```
PATH=$PATH:$SIMULATE_PATH/simulate python fiCity.py
```

## control

A node js app pushing house data to the context broker

Run with:

```
node control [hostname of orion]
```
