# based on image "gitlab-ci-multi-runner (based on Ubuntu 16.04 Cloud Server)"
sudo apt-get update \
	&& sudo apt-get install -y docker.io \
	&& sudo chmod u+s `which docker` \
	&& for image in \
		gcc \
		python:alpine \
		node:alpine \
		fiware/orion
	do docker pull $image || exit $?
	done \
	&& if [[ -d awesome-erc ]]
	then
		cd awesome-erc \
		&& git fetch \
		&& git fetch --tags
	else
		git clone -b tutorial-01 https://git.rwth-aachen.de/yushin.washio/awesome-erc.git \
		&& cd awesome-erc
	fi && (
		git checkout my-tutorial \
		|| git checkout -b my-tutorial tutorial-01
	) \
	&& mkdir -p ~/.vim \
	&& if ! grep -q shiftwidth ~/.vim/vimrc
	then tee ~/.vim/vimrc <<< 'set expandtab
set shiftwidth=2
set tabstop=2
set autoindent
set smartindent'
	fi
